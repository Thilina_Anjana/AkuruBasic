/*
 * This file is part of Akuru (Basic).
 *
 * Akuru (Basic) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * Akuru (Basic) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Akuru (Basic).  If not, see <https://www.gnu.org/licenses/>.
 */

package akuru;

import android.animation.Animator;
import android.content.Intent;
import android.graphics.RectF;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewTreeObserver;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.widget.TooltipCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.LinearSmoothScroller;

import akuru.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {
    private ActivityMainBinding viewBinding;
    private final RectF viewport = new RectF(0F, 0F, 150F, 100F);
    private boolean moreRules = false;
    private boolean autoPaused = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        super.onCreate(savedInstanceState);
        viewBinding = ActivityMainBinding.inflate(getLayoutInflater());

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        LinearSmoothScroller linearSmoothScroller = new LinearSmoothScroller(viewBinding.list.getContext()) {
            @Override
            protected float calculateSpeedPerPixel(DisplayMetrics displayMetrics) {
                return 150F / displayMetrics.densityDpi;
            }
        };
        linearSmoothScroller.setTargetPosition(0);
        LettersRVAdapter recyclerViewAdapter = new LettersRVAdapter();
        recyclerViewAdapter.setOnItemClickListener(this::setLetter);
        viewBinding.list.setLayoutManager(linearLayoutManager);
        viewBinding.list.setAdapter(recyclerViewAdapter);

        viewBinding.getRoot().getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                int rootWidth = viewBinding.getRoot().getMeasuredWidth();
                viewBinding.getRoot().getViewTreeObserver().removeOnGlobalLayoutListener(this);
                viewBinding.list.getLayoutParams().height = (int) (rootWidth / 4.5);
                viewBinding.svgContainer.getLayoutParams().height = rootWidth / 3 * 2;
                viewBinding.buttonContainer.getLayoutParams().height = rootWidth / 5;
                viewBinding.list.scrollToPosition(4);
                viewBinding.getRoot().requestLayout();
                new Handler(Looper.getMainLooper()).postDelayed(() -> linearLayoutManager.startSmoothScroll(linearSmoothScroller), 300);
            }
        });

        viewBinding.rules.setViewportSize(viewport);
        setRules();
        viewBinding.distances.setViewportSize(viewport);
        viewBinding.distances.setGlyph(new Glyph(getResources().getStringArray(R.array.distances_paths), viewport.width()));
        viewBinding.distances.setStrokeColor(ResourcesCompat.getColor(getResources(), R.color.path_black, getTheme()));
        viewBinding.distances.setStrokeWidth(1.5F);
        viewBinding.distances.rebuildGlyphData();
        viewBinding.distances.setToFinishedFrame();
        viewBinding.preview.setViewportSize(viewport);
        viewBinding.letter.setViewportSize(viewport);
        viewBinding.letter.setOnStateChangeListener(state -> {
            switch (state) {
                case PathAnimator.STATE_NOT_STARTED:
                case PathAnimator.STATE_FINISHED:
                case PathAnimator.STATE_PAUSED:
                    viewBinding.buttonPlay.setImageResource(R.drawable.btn_ic_play);
                    setDescription(viewBinding.buttonPlay, getString(R.string.description_play));
                    break;
                case PathAnimator.STATE_PLAYING:
                    autoPaused = false;
                    viewBinding.buttonPlay.setImageResource(R.drawable.btn_ic_pause);
                    setDescription(viewBinding.buttonPlay, getString(R.string.description_pause));
                    break;
            }
        });
        setLetter(56);

        viewBinding.buttonDistances.setOnClickListener((v -> {
            toggleAbout(true);
            String newDescription;
            if (viewBinding.distances.getVisibility() == View.VISIBLE) {
                viewBinding.distances.setVisibility(View.GONE);
                newDescription = getString(R.string.description_distances_show);
            } else {
                viewBinding.distances.setVisibility(View.VISIBLE);
                newDescription = getString(R.string.description_distances_hide);
            }
            setDescription(v, newDescription);
        }));
        setDescription(viewBinding.buttonDistances, getString(R.string.description_distances_show));
        viewBinding.buttonRules.setOnClickListener((v -> {
            toggleAbout(true);
            moreRules = !moreRules;
            setRules();
        }));
        setDescription(viewBinding.buttonRules, getString(R.string.description_rules_show));
        viewBinding.buttonPlay.setOnClickListener((v -> {
            toggleAbout(true);
            int state = viewBinding.letter.getState();
            if (state == PathAnimator.STATE_PLAYING) {
                viewBinding.letter.pause();
                autoPaused = false;
            } else {
                viewBinding.letter.start();
            }
        }));
        setDescription(viewBinding.buttonPlay, getString(R.string.description_play));
        viewBinding.buttonReset.setOnClickListener((v -> {
            toggleAbout(true);
            viewBinding.letter.setToFinishedFrame();
        }));
        setDescription(viewBinding.buttonReset, getString(R.string.description_reset));
        viewBinding.buttonAbout.setOnClickListener((v -> toggleAbout(false)));
        setDescription(viewBinding.buttonAbout, getString(R.string.description_about_show));

        viewBinding.sourceLink.setOnClickListener((v ->
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://codeberg.org/sinhala/AkuruBasic")))));
        viewBinding.licenseLink.setOnClickListener((v ->
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.gnu.org/licenses/gpl-3.0.txt")))));

        setContentView(viewBinding.getRoot());
        getWindow().setBackgroundDrawableResource(R.color.bg_blue);
    }

    private void setDescription(View view, String description) {
        view.setContentDescription(description);
        TooltipCompat.setTooltipText(view, description);
    }

    private void toggleAbout(boolean hideOnly) {
        boolean isVisible = viewBinding.about.getVisibility() == View.VISIBLE;
        if (!isVisible && hideOnly) return;
        int cx = viewBinding.svgContainer.getWidth() / 2;
        int cy = viewBinding.svgContainer.getHeight() / 2;
        float hypotenuse = (float) Math.hypot(cx, cy);
        float startRadius;
        float endRadius;
        String newDescription;
        if (isVisible) {
            startRadius = hypotenuse;
            endRadius = 0;
            newDescription = getString(R.string.description_about_show);
        } else {
            startRadius = 0;
            endRadius = hypotenuse;
            newDescription = getString(R.string.description_about_hide);
        }
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            Animator animator = ViewAnimationUtils.createCircularReveal(viewBinding.about, cx, cy, startRadius, endRadius);
            animator.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {
                    if (!isVisible) {
                        if (viewBinding.letter.getState() == PathAnimator.STATE_PLAYING) {
                            viewBinding.letter.pause();
                            autoPaused = true;
                        }
                        viewBinding.about.setVisibility(View.VISIBLE);
                    }
                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    if (isVisible) {
                        viewBinding.about.setVisibility(View.INVISIBLE);
                        if (autoPaused && viewBinding.letter.getState() == PathAnimator.STATE_PAUSED) viewBinding.letter.start();
                    }
                    setDescription(viewBinding.buttonAbout, newDescription);
                }

                @Override
                public void onAnimationCancel(Animator animation) {
                }

                @Override
                public void onAnimationRepeat(Animator animation) {
                }
            });
            animator.start();
        } else {
            if (isVisible) {
                viewBinding.about.setVisibility(View.INVISIBLE);
                if (autoPaused && viewBinding.letter.getState() == PathAnimator.STATE_PAUSED) viewBinding.letter.start();
            } else {
                if (viewBinding.letter.getState() == PathAnimator.STATE_PLAYING) {
                    viewBinding.letter.pause();
                    autoPaused = true;
                }
                viewBinding.about.setVisibility(View.VISIBLE);
            }
            setDescription(viewBinding.buttonAbout, newDescription);
        }
    }

    private void setRules() {
        String[] paths;
        int[] strokeColors;
        String newDescription;
        if (moreRules) {
            paths = new String[]{
                    "m0 10h" + viewport.width(),
                    "m0 20h" + viewport.width(),
                    "m0 30h" + viewport.width(),
                    "m0 40h" + viewport.width(),
                    "m0 50h" + viewport.width(),
                    "m0 60h" + viewport.width(),
                    "m0 70h" + viewport.width(),
                    "m0 80h" + viewport.width(),
                    "m0 90h" + viewport.width()
            };
            strokeColors = new int[]{
                    ResourcesCompat.getColor(getResources(), R.color.path_red, getTheme()),
                    ResourcesCompat.getColor(getResources(), R.color.path_gray, getTheme()),
                    ResourcesCompat.getColor(getResources(), R.color.path_blue, getTheme()),
                    ResourcesCompat.getColor(getResources(), R.color.path_blue, getTheme()),
                    ResourcesCompat.getColor(getResources(), R.color.path_gray, getTheme()),
                    ResourcesCompat.getColor(getResources(), R.color.path_gray, getTheme()),
                    ResourcesCompat.getColor(getResources(), R.color.path_blue, getTheme()),
                    ResourcesCompat.getColor(getResources(), R.color.path_gray, getTheme()),
                    ResourcesCompat.getColor(getResources(), R.color.path_red, getTheme())
            };
            newDescription = getString(R.string.description_rules_hide);
        } else {
            paths = new String[]{
                    "m0 10h" + viewport.width(),
                    "m0 30h" + viewport.width(),
                    "m0 40h" + viewport.width(),
                    "m0 70h" + viewport.width(),
                    "m0 90h" + viewport.width()
            };
            strokeColors = new int[]{
                    ResourcesCompat.getColor(getResources(), R.color.path_red, getTheme()),
                    ResourcesCompat.getColor(getResources(), R.color.path_blue, getTheme()),
                    ResourcesCompat.getColor(getResources(), R.color.path_blue, getTheme()),
                    ResourcesCompat.getColor(getResources(), R.color.path_blue, getTheme()),
                    ResourcesCompat.getColor(getResources(), R.color.path_red, getTheme())
            };
            newDescription = getString(R.string.description_rules_show);
        }
        viewBinding.rules.setGlyph(new Glyph(paths, viewport.width()));
        viewBinding.rules.setStrokeColors(strokeColors);
        viewBinding.rules.rebuildGlyphData();
        viewBinding.rules.setToFinishedFrame();
        setDescription(viewBinding.buttonRules, newDescription);
    }

    private void setLetter(int letterId) {
        toggleAbout(true);
        viewBinding.preview.setGlyph(Akuru.values()[letterId].getGlyph());
        viewBinding.preview.setStrokeColor(ResourcesCompat.getColor(getResources(), R.color.path_gray, getTheme()));
        viewBinding.preview.rebuildGlyphData();
        viewBinding.preview.setToFinishedFrame();
        viewBinding.letter.setGlyph(Akuru.values()[letterId].getGlyph());
        viewBinding.letter.setStrokeColor(ResourcesCompat.getColor(getResources(), R.color.path_black, getTheme()));
        viewBinding.letter.rebuildGlyphData();
        viewBinding.letter.setToFinishedFrame();
    }
}
