/*
 * This file is part of Akuru (Basic).
 *
 * Akuru (Basic) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * Akuru (Basic) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Akuru (Basic).  If not, see <https://www.gnu.org/licenses/>.
 */

package akuru;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.TooltipCompat;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import akuru.databinding.ListItemBinding;

public class LettersRVAdapter extends RecyclerView.Adapter<LettersRVAdapter.ViewHolder> {
    private final int[] thumbnails = {
            R.drawable.l1, R.drawable.l2, R.drawable.l3, R.drawable.l4, R.drawable.l5, R.drawable.l6,
            R.drawable.l7, R.drawable.l8, R.drawable.l9, R.drawable.l10, R.drawable.l11, R.drawable.l12,
            R.drawable.l13, R.drawable.l14, R.drawable.l15, R.drawable.l16, R.drawable.l17, R.drawable.l18,
            R.drawable.l19, R.drawable.l20,
            R.drawable.l21, R.drawable.l22, R.drawable.l23, R.drawable.l24, R.drawable.l25, R.drawable.l26,
            R.drawable.l27, R.drawable.l28, R.drawable.l29, R.drawable.l30, R.drawable.l31, R.drawable.l32,
            R.drawable.l33, R.drawable.l34, R.drawable.l35, R.drawable.l36, R.drawable.l37, R.drawable.l38,
            R.drawable.l39, R.drawable.l40, R.drawable.l41, R.drawable.l42, R.drawable.l43, R.drawable.l44,
            R.drawable.l45, R.drawable.l46, R.drawable.l47, R.drawable.l48, R.drawable.l49, R.drawable.l50,
            R.drawable.l51, R.drawable.l52, R.drawable.l53, R.drawable.l54,
            R.drawable.l55, R.drawable.l56, R.drawable.l57, R.drawable.l58, R.drawable.l59,
            R.drawable.l60, R.drawable.l61, R.drawable.l62
    };
    String[] letters;
    OnItemClickListener onItemClickListener;

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        letters = new String[Akuru.values().length];
        for (int i = 0; i < Akuru.values().length; i++) letters[i] = Akuru.values()[i].name;
        super.onAttachedToRecyclerView(recyclerView);
    }

    public interface OnItemClickListener {
        void clickListener(int position);
    }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new LettersRVAdapter.ViewHolder(ListItemBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Context context = holder.viewBinding.getRoot().getContext();
        holder.viewBinding.getRoot().setImageResource(thumbnails[position]);
        holder.viewBinding.getRoot().setBackgroundResource(R.drawable.bg_list_item);
        holder.viewBinding.getRoot().setOnClickListener((v -> onItemClickListener.clickListener(position)));
        setDescription(
                holder.viewBinding.getRoot(),
                String.format(
                        context.getString(R.string.description_letter),
                        letters[position]
                ));
    }

    private void setDescription(View view, String description) {
        view.setContentDescription(description);
        TooltipCompat.setTooltipText(view, description);
    }

    @Override
    public int getItemCount() {
        return thumbnails.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        ListItemBinding viewBinding;

        public ViewHolder(@NonNull ListItemBinding viewBinding) {
            super(viewBinding.getRoot());
            this.viewBinding = viewBinding;
        }
    }
}
