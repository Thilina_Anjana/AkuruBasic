/*
 * This file is part of Akuru (Basic).
 *
 * Akuru (Basic) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * Akuru (Basic) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Akuru (Basic).  If not, see <https://www.gnu.org/licenses/>.
 */

package akuru;

public final class Glyph {
    String[] paths;
    Float width;

    public Glyph(String[] p, Float w) {
        paths = p;
        width = w;
    }

    public String[] getPaths() {
        return paths;
    }

    public float getWidth() {
        return width;
    }
}
