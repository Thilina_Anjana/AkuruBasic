/*
 * This file is part of Akuru (Basic).
 *
 * Akuru (Basic) is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * Akuru (Basic) is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Akuru (Basic).  If not, see <https://www.gnu.org/licenses/>.
 */

package akuru;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PathMeasure;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;

import androidx.annotation.ColorInt;
import androidx.annotation.IntDef;
import androidx.core.graphics.PathParser;
import androidx.core.view.ViewCompat;

public class PathAnimator extends View {

    public static final int STATE_NOT_STARTED = 0;
    public static final int STATE_PLAYING = 1;
    public static final int STATE_PAUSED = 2;
    public static final int STATE_FINISHED = 3;

    private static float constrain(float v) {
        return Math.max(0, Math.min(1, v));
    }

    private int totalTraceTime = 0;
    private int[] pathStrokeColors;
    private RectF viewport;
    private GlyphData[] glyphDataArray;
    private Glyph glyph;
    private int mWidth;
    private int mHeight;
    private long mStartTime;
    private long mSavedTime;
    private final Paint defaultPaint = new Paint();

    private int mState = STATE_NOT_STARTED;
    private OnStateChangeListener mOnStateChangeListener;

    public PathAnimator(Context context) {
        super(context);
        init();
    }

    public PathAnimator(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public PathAnimator(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init() {
        pathStrokeColors = new int[1];
        pathStrokeColors[0] = 0x32000000;
        defaultPaint.setStyle(Paint.Style.STROKE);
        defaultPaint.setStrokeCap(Paint.Cap.ROUND);
        defaultPaint.setStrokeJoin(Paint.Join.ROUND);
        defaultPaint.setColor(Color.WHITE);
        defaultPaint.setAntiAlias(true);
        defaultPaint.setStrokeWidth(
                TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 4F, getResources().getDisplayMetrics())
        );

        // Note: using a software layer here is an optimization. This view works with hardware accelerated rendering but
        // every time a path is modified (when the dash path effect is modified), the graphics pipeline will rasterize
        // the path again in a new texture. Since we are dealing with dozens of paths, it is much more efficient to
        // rasterize the entire view into a single re-usable texture instead. Ideally this should be toggled using a
        // heuristic based on the number and or dimensions of paths to render.
        setLayerType(LAYER_TYPE_SOFTWARE, null);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        mWidth = w;
        mHeight = h;
        rebuildGlyphData();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int width = View.MeasureSpec.getSize(widthMeasureSpec);
        int height = View.MeasureSpec.getSize(heightMeasureSpec);
        int widthMode = View.MeasureSpec.getMode(widthMeasureSpec);
        int heightMode = View.MeasureSpec.getMode(heightMeasureSpec);

        if (height <= 0 && width <= 0 && heightMode == View.MeasureSpec.UNSPECIFIED && widthMode == View.MeasureSpec.UNSPECIFIED) {
            width = 0;
            height = 0;
        } else if (height <= 0 && heightMode == View.MeasureSpec.UNSPECIFIED) {
            height = (int) (width * viewport.height() / viewport.width());
        } else if (width <= 0 && widthMode == View.MeasureSpec.UNSPECIFIED) {
            width = (int) (height * viewport.width() / viewport.height());
        } else if (width * viewport.height() > viewport.width() * height) {
            width = (int) (height * viewport.width() / viewport.height());
        } else {
            height = (int) (width * viewport.height() / viewport.width());
        }
        super.onMeasure(
                MeasureSpec.makeMeasureSpec(width, MeasureSpec.EXACTLY),
                MeasureSpec.makeMeasureSpec(height, MeasureSpec.EXACTLY)
        );
    }

    @SuppressLint("DrawAllocation")
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (mState == STATE_NOT_STARTED) return;

        long t = System.currentTimeMillis() - mStartTime;
        for (int i = 0; i < glyphDataArray.length; i++) {
            float phase = constrain((float) (t - glyphDataArray[i].traceStartDelay) / glyphDataArray[i].traceTime);
            float distance = phase * glyphDataArray[i].length;
            glyphDataArray[i].paint = defaultPaint;
            glyphDataArray[i].paint.setColor(pathStrokeColors[i]);
            if (t >= glyphDataArray[i].traceStartDelay) {
                float[] intervals = {distance, glyphDataArray[i].length};
                glyphDataArray[i].paint.setPathEffect(new DashPathEffect(intervals, 0F));
                canvas.drawPath(glyphDataArray[i].path, glyphDataArray[i].paint);
            }
        }

        if (t < totalTraceTime && mState == STATE_PLAYING) {
            ViewCompat.postInvalidateOnAnimation(this);
        } else if (mState != STATE_PAUSED) changeState(STATE_FINISHED);
    }

    private int calculateTraceTime(Float length) {
        try {
            float x = mWidth / 32F;
            float y = mHeight / 32F;
            Matrix scaleMatrix = new Matrix();
            scaleMatrix.setScale(x, y);
            Path path = PathParser.createPathFromPathData("m0 16h32");
            path.transform(scaleMatrix);
            PathMeasure pm = new PathMeasure(path, true);
            Float speed = 10000 / pm.getLength() / 1;
            return (int) (length * speed);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 4000;
    }

    public void rebuildGlyphData() {
        totalTraceTime = 0;
        float x = mWidth / viewport.width();
        float y = mHeight / viewport.height();
        Matrix scaleMatrix = new Matrix();
        scaleMatrix.setScale(x, y);

        glyphDataArray = new GlyphData[glyph.getPaths().length];
        float offset = (viewport.width() - glyph.getWidth()) / 2;

        for (int i = 0; i < glyph.getPaths().length; i++) {
            String pathString = glyph.getPaths()[i];
            Path path = PathParser.createPathFromPathData(pathString);
            Matrix translateMatrix = new Matrix();
            translateMatrix.setTranslate(offset, 0F);
            path.transform(translateMatrix);
            glyphDataArray[i] = new GlyphData();
            glyphDataArray[i].path = path;
        }

        int delayBetweenPaths = 250;
        int traceStartDelay = 0;
        for (GlyphData glyphData : glyphDataArray) {
            glyphData.path.transform(scaleMatrix);

            PathMeasure pm = new PathMeasure(glyphData.path, false);

            do glyphData.length = Math.max(glyphData.length, pm.getLength());
            while (pm.nextContour());

            int pathTraceTime = calculateTraceTime(glyphData.length);

            totalTraceTime += pathTraceTime;
            glyphData.traceTime = pathTraceTime;
            glyphData.traceStartDelay = traceStartDelay;
            traceStartDelay += pathTraceTime + delayBetweenPaths;
            glyphData.paint = new Paint(defaultPaint);
        }
        totalTraceTime += delayBetweenPaths * (glyphDataArray.length - 1);
    }

    public void setViewportSize(RectF v) {
        viewport = v;
        requestLayout();
    }

    public void setGlyph(Glyph g) {
        glyph = g;
        rebuildGlyphData();
    }

    public void setStrokeColors(int[] colors) {
        pathStrokeColors = colors;
    }

    public void setStrokeColor(@ColorInt int color) {
        int length = glyphDataArray.length;
        int[] colors = new int[length];
        for (int i = 0; i < length; i++) colors[i] = color;
        setStrokeColors(colors);
    }

    public void setStrokeWidth(float w) {
        defaultPaint.setStrokeWidth(
                TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, w, getResources().getDisplayMetrics())
        );
    }

    public void start() {
        if (getState() == STATE_PAUSED) {
            mStartTime = System.currentTimeMillis() - mSavedTime;
        } else {
            mStartTime = System.currentTimeMillis();
        }
        changeState(STATE_PLAYING);
        ViewCompat.postInvalidateOnAnimation(this);
    }

    public void pause() {
        mSavedTime = System.currentTimeMillis() - mStartTime;
        changeState(STATE_PAUSED);
    }

    public void setToFinishedFrame() {
        mStartTime = 1;
        changeState(STATE_FINISHED);
        ViewCompat.postInvalidateOnAnimation(this);
    }

    @State
    public int getState() {
        return mState;
    }

    public void setOnStateChangeListener(OnStateChangeListener onStateChangeListener) {
        mOnStateChangeListener = onStateChangeListener;
    }

    private void changeState(@State int state) {
        if (mState == state) {
            return;
        }

        mState = state;
        if (mOnStateChangeListener != null) {
            mOnStateChangeListener.onStateChange(state);
        }
    }

    public interface OnStateChangeListener {
        void onStateChange(@State int state);
    }

    @IntDef({STATE_NOT_STARTED, STATE_PLAYING, STATE_PAUSED, STATE_FINISHED})
    public @interface State {
    }

    static final class GlyphData {
        Path path;
        Paint paint;
        float length;
        int traceTime;
        int traceStartDelay;
    }

}
