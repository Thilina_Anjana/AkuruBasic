# අකුරු (මූලික)<br>Akuru (Basic)

සිංහල අකුරු ලිවීම සඳහා හඳුන්වා දී ඇති 'පස්රූල' මත අකුරු ලියන ආකාරය පෙන්වන යෙදුමකි.

### පින්තූර
<img src="metadata/android/en-US/images/phoneScreenshots/1.png" width="150">
<img src="metadata/android/en-US/images/phoneScreenshots/2.png" width="150">
<img src="metadata/android/en-US/images/phoneScreenshots/3.png" width="150">

### ස්තුතිය
• <a href="https://f-droid.org/packages/kasun.sinhala.keyboard/" rel="noopener noreferrer nofollow">github.com/jaredrummler/AnimatedSvgView</a>

### බලපත්‍රය
GNU General Public License v3.0 only

```
Copyright (C) 2021 Kasun

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, version 3.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.
```
